#!/system/bin/sh

#================================================================
#   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
#   
#   filename：format.sh
#   auther  ：wuyingzhen
#   create  ：2022-03-04
#   someother：
#
#================================================================

astyle  --style=google  --recursive  /home/wyz/gitspace/parrot_net/src/*.cpp,*.h
