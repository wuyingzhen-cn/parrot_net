# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wyz/gitspace/parrot_net/src/agent.cpp" "/home/wyz/gitspace/parrot_net/CMakeFiles/parrot_net.dir/src/agent.cpp.o"
  "/home/wyz/gitspace/parrot_net/src/agentManager.cpp" "/home/wyz/gitspace/parrot_net/CMakeFiles/parrot_net.dir/src/agentManager.cpp.o"
  "/home/wyz/gitspace/parrot_net/src/client.cpp" "/home/wyz/gitspace/parrot_net/CMakeFiles/parrot_net.dir/src/client.cpp.o"
  "/home/wyz/gitspace/parrot_net/src/package_protocal.cpp" "/home/wyz/gitspace/parrot_net/CMakeFiles/parrot_net.dir/src/package_protocal.cpp.o"
  "/home/wyz/gitspace/parrot_net/src/server.cpp" "/home/wyz/gitspace/parrot_net/CMakeFiles/parrot_net.dir/src/server.cpp.o"
  "/home/wyz/gitspace/parrot_net/src/socket_stream.cpp" "/home/wyz/gitspace/parrot_net/CMakeFiles/parrot_net.dir/src/socket_stream.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "parrot_net_EXPORTS"
  "std_cxx_20"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
