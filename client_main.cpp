/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：main.cpp
*   auther  ：wuyingzhen
*   create  ：2022-03-03
*   someother：
*
================================================================*/
#include <cstdio>
#include <iostream>

//#include "server.h"
#include "client.h"

#define ASIO_ENABLE_HANDLER_TRACKING 


void on_connect(Parrot::Net::client_ptr){
  printf("on_connect...\n");
}

void on_close(Parrot::Net::client_ptr){
  printf("on_close...\n");
}

void on_message(asio::mutable_buffer,Parrot::Net::client_ptr){
  printf("on_message...\n");
}

int main(int argc,char ** argv){
printf("hello,world\n");
try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: async_tcp_echo_client <port>\n";
      return 1;
    }
    
    asio::io_context io_context;

    //Parrot::Net::Client c(io_context, argv[1],std::atoi(argv[2]));

    auto c = std::make_shared<Parrot::Net::Client>(io_context,"127.0.0.1",std::atoi(argv[1]));
    c->open(on_connect,on_close,on_message);

    io_context.run();
    
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }
  printf("happy,ending\n");
  return 0;
}