/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：agent.h
*   auther  ：wuyingzhen
*   create  ：2022-03-03
*   someother：
*
================================================================*/

#ifndef _AGENT_H
#define _AGENT_H

#include <memory>
#include <unordered_map>
#include <list>
#define ASIO_ENABLE_HANDLER_TRACKING 
#include "asio/detail/noncopyable.hpp"
#include "asio/ip/tcp.hpp"

#include "socket_stream.h"

namespace Parrot {
namespace Net {

//pwd
class Agent;
using agent_ptr = std::shared_ptr<Agent>;

class Agent
  : public asio::detail::noncopyable,
  public std::enable_shared_from_this<Agent> {
   public:
    using on_connect = std::function<void(agent_ptr)>;
    using on_close = std::function<void(agent_ptr)>;
    using on_message = std::function<void(asio::mutable_buffer,agent_ptr)>;
    Agent(asio::ip::tcp::socket socket);
    ~Agent() = default;
    bool open(on_connect,on_close,on_message);

    
  
  std::size_t  id_;
 private:
  on_connect on_connect_;
  on_close on_close_;
  on_message on_message_;
  std::unique_ptr<socket_stream> stream_;
};

} // end namespace Net
} // end namespace Parrot

#endif // AGENT_H
