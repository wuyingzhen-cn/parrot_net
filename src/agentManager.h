/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：agentManager.h
*   auther  ：wuyingzhen
*   create  ：2022-04-06
*   someother：
*
================================================================*/

#ifndef _AGENTMANAGER_H
#define _AGENTMANAGER_H

#include "agent.h"


namespace Parrot {
namespace Net {

//管理agent
class AgentManager
  : public asio::detail::noncopyable {
   public:
   AgentManager()=default;
   ~AgentManager()=default;

  bool add_agent(std::shared_ptr<Agent> new_agent);
    
 private:
  std::unordered_map<size_t, std::shared_ptr<Agent>> agents_;
  size_t agent_id_ = 0;
};

} // end namespace Net
} // end namespace Parrot


#endif //AGENTMANAGER_H
