/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：agentManager.cpp
*   auther  ：wuyingzhen
*   create  ：2022-04-06
*   someother：
*
================================================================*/

#include "agentManager.h"

namespace Parrot {
namespace Net {

bool AgentManager::add_agent(std::shared_ptr<Agent> new_agent) {
  new_agent->id_ = ++agent_id_;
  agents_[new_agent->id_] = new_agent;
  printf("new agent [%ld]\n",new_agent->id_);
  return true;
}


} // end namespace Net
} // end namespace Parrot
