/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：agent.cpp
*   auther  ：wuyingzhen
*   create  ：2022-03-03
*   someother：
*
================================================================*/

#include "agent.h"
#include "package_protocal.h"


namespace Parrot {
namespace Net {

Agent::Agent(asio::ip::tcp::socket socket){
  stream_ = std::make_unique<socket_stream>(std::move(socket));
}
  
bool Agent::open(on_connect connect_func,on_close close_func,on_message message_func){
  on_connect_ = connect_func;
  on_close_ = close_func;
  on_message_ =  message_func;
  auto self(shared_from_this());
        
           stream_->open([this,self]{
               on_connect_(self);
           },[this,self]{
               on_close_(self);
           },package_parser_EOF);
  return true;
}





} // end namespace Net
} // end namespace Parrot