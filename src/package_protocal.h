/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：package_protocal.h
*   auther  ：wuyingzhen
*   create  ：2022-04-06
*   someother：
*
================================================================*/

#ifndef _PACKAGE_PROTOCAL_H
#define _PACKAGE_PROTOCAL_H

#include "buffer_storage.h"
#include "asio/buffer.hpp"

std::pair<bool,std::size_t> package_parser_EOF(asio::const_buffer buff);


#endif //PACKAGE_PROTOCAL_H
