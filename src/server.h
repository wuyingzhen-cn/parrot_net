/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：Server.h
*   auther  ：wuyingzhen
*   create  ：2022-03-03
*   someother：
*
================================================================*/

#ifndef _Server_H
#define _Server_H
#define ASIO_ENABLE_HANDLER_TRACKING 
#include "asio/detail/noncopyable.hpp"
#include "asio/io_context.hpp"
#include "asio/ip/tcp.hpp"
#include "asio/buffer.hpp"
#include "asio/write.hpp"


#include "agentManager.h"

namespace Parrot {
namespace Net {

class Server
  : public asio::detail::noncopyable {
   public:
    

    Server(asio::io_context& io_context, unsigned short port);
    ~Server() = default;
    void open(Agent::on_connect,Agent::on_close,Agent::on_message);
    void close();


 private:
  void do_accept();
  asio::ip::tcp::acceptor acceptor_;

  Agent::on_connect on_connect_;
  Agent::on_close on_close_;
  Agent::on_message on_message_;
 
  AgentManager agentManager_;
};


} // end namespace Net
}  // end namespace Parrot



#endif //Server_H
