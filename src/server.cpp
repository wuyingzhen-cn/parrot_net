/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：server.cpp
*   auther  ：wuyingzhen
*   create  ：2022-03-03
*   someother：
*
================================================================*/

#include "server.h"


namespace Parrot {
namespace Net {

Server::Server(asio::io_context& io_context, unsigned short port) :
  acceptor_(io_context, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)){
 
}

void Server::open(Agent::on_connect connect_func,Agent::on_close close_func,Agent::on_message message_func){
  on_connect_ = connect_func;
  on_close_ = close_func;
  on_message_ = message_func;
   do_accept();
}


void Server::do_accept() {
  acceptor_.async_accept(
      [this](std::error_code ec, asio::ip::tcp::socket socket){
        // Check whether the server was stopped by a signal before this
        // completion handler had a chance to run.
        if (!acceptor_.is_open()){
          return;
        }

        if (!ec){
          auto agent = std::make_shared<Agent>(std::move(socket));
          agent->open(on_connect_,on_close_,on_message_);
          agentManager_.add_agent(agent);
        }

        do_accept();
      });
}

} // end namespace Net
} // end namespace Parrot