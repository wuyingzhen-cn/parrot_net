/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：package_protocal.cpp
*   auther  ：wuyingzhen
*   create  ：2022-04-06
*   someother：
*
================================================================*/

#include "package_protocal.h"

std::pair<bool,std::size_t> package_parser_EOF(asio::const_buffer buff){
    bool has_message = false;
    std::size_t length = 0;
    if(!buff.data()){
        return std::make_pair<>(has_message,length);
    }
    
    std::vector<std::pair<std::size_t,std::size_t>> packages;

    const char* data = static_cast<const char*>(buff.data());
    std::size_t begin = 0, end = 0;
    for(std::size_t i = 0; i < buff.size();i++){
        char ch = data[i];
        if(ch == 'e'){
            end = i;
            packages.push_back(std::make_pair<>(begin,end));
            //printf
            std::vector<char> printData(data[begin],(end-begin+1)); 
            printf("msg:%s\n",printData.data());   
            begin = end+1;
            has_message = true;
            length = i+1;
        }
        
    }
    return std::make_pair<>(has_message,length);
}
