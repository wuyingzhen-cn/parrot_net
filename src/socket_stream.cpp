/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：socket_stream.cpp
*   auther  ：wuyingzhen
*   create  ：2022-03-25
*   someother：
*
================================================================*/

#include "socket_stream.h"

namespace Parrot {
namespace Net {

#define READ_BUFFER_SIZE (16)

socket_stream::socket_stream(asio::ip::tcp::socket socket) :
  socket_(std::move(socket)),
  is_sending_(false),
  status_(SOCKET_CONNECT_STATUS_INIT) {
}

bool socket_stream::open(on_connect connect_func,on_close close_func,package_parse parser) {
  on_connect_ = connect_func;
  on_close_ = close_func;
  package_parse_ = parser;

  read_buffer_ = std::make_shared<buffer_t>(READ_BUFFER_SIZE);
 
  on_connect_();
  do_read();
  return true;
}

void socket_stream::do_read() {
  try {
   
    socket_.async_read_some(asio::buffer(read_buffer_->buff(),read_buffer_->buff_length()),
                            [this](std::error_code ec, std::size_t bytes_transferred) {
                              if(on_read(ec,bytes_transferred)) {
                                do_read();
      }
    });
  } catch (...) {
    //异步读报错
    stop();
    return;
  }
}

bool socket_stream::on_read(std::error_code ec, std::size_t bytes_transferred) {
  printf("on_read:ec[%d],bytes_transferred:[%ld]\n",ec.value(),bytes_transferred);
  if (!ec) {
    if(bytes_transferred > 0) {
      read_buffer_->fill(bytes_transferred);
      printf("data:[%d][%s]\n",read_buffer_->size(),read_buffer_->data());

     
      auto result = package_parse_(asio::buffer(read_buffer_->data(),read_buffer_->size()));
      printf("parse result:[%d][%d]\n",result.first,result.second);
      if(result.first) {

          auto len = result.second;
          read_buffer_->consume(len);
        
      }

      return true;
    } else {
      stop();
      return false;
    }
  } else if (ec != asio::error::operation_aborted) {
    stop();
    return false;
  } else {
    return true;
  }

}

void socket_stream::do_write() {
  if(!is_sending_ && send_buffer_pool_.empty()) {
    is_sending_ = true;
    current_send_buffer_ = send_buffer_pool_.front();

    try {
     
      socket_.async_write_some(asio::buffer(current_send_buffer_->data(), current_send_buffer_->size()),
                               [this](std::error_code ec, std::size_t bytes_transferred) {
                                 on_write(ec,bytes_transferred);
      });
    } catch(...) {
      //写出错
      stop();
    }
  }
}

bool socket_stream::on_write(std::error_code ec, std::size_t bytes_transferred) {
  if (!ec) {
    current_send_buffer_->consume(bytes_transferred);
    if(current_send_buffer_->empty()) {
      send_buffer_pool_.pop_front();
      is_sending_ = false;
      do_write();
    } else {
      try {
       
        socket_.async_write_some(asio::buffer(current_send_buffer_->data(), current_send_buffer_->size()),
                                 [this](std::error_code ec, std::size_t bytes_transferred) {
                                   on_write(ec,bytes_transferred);
        });
      } catch(...) {
        //写出错
        stop();
      }

    }

  } else {
    stop();
    return false;
  }
}


void socket_stream::stop() {
  try {
    printf("stop...\n");
    std::error_code error;
    socket_.shutdown(asio::socket_base::shutdown_both, error);
    if(error) {
      //shutdown 失败
    }

    socket_.close(error);
    on_close_();
    if(error) {
      //关闭失败
    }
  } catch(...) {
    //stop 报错
  }
}

} // end namespace Net
} // end namespace Parrot