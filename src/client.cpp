/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：client.cpp
*   auther  ：wuyingzhen
*   create  ：2022-04-06
*   someother：
*
================================================================*/

#include "client.h"
#include "package_protocal.h"

namespace Parrot {
namespace Net {

Client::Client(asio::io_context& io_context,std::string ip,unsigned short port):
    ip_(ip),port_(port),io_context_(io_context),socket_(io_context_)
{
    
}

void Client::open(on_connect connect_func,on_close close_func,on_message message_func){
    on_connect_ = connect_func;
    on_close_ = close_func;
    on_message_ = message_func;
    do_connect();
}

bool Client::do_connect(){
   asio::ip::tcp::endpoint endpoint(asio::ip::address_v4::from_string(ip_), port_); 
   auto self(shared_from_this());
   
   socket_.async_connect(endpoint,[this,self](const asio::error_code& ec){
       if(ec){
           //connect fail

       }else{
           //connect succ
           stream_ = std::make_unique<socket_stream>(std::move(socket_));
           stream_->open([this,self]{
               on_connect_(self);
           },[this,self]{
               on_close_(self);
           },package_parser_EOF);
       }
   });

}


} // end namespace Net
}  // end namespace Parrot