/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：buffer_storage.h
*   auther  ：wuyingzhen
*   create  ：2022-03-25
*   someother：
*
================================================================*/

#ifndef _BUFFER_STORAGE_H
#define _BUFFER_STORAGE_H

#include <cctype>
#include <cstdarg>
#include <cstring>
#include <cassert>
#include <memory>
#include <vector>

namespace Parrot {
namespace Net {

class buffer_storage
{
public:
  // The type of the bytes stored in the buffer.
  typedef unsigned char byte_type;
  typedef byte_type* byte_type_pointer;
  typedef const byte_type* byte_type_const_pointer;

  // The type used for offsets into the buffer.
  typedef std::size_t size_type;

  // Constructor.
  explicit buffer_storage(std::size_t buffer_capacity)
    : begin_offset_(0),
      end_offset_(0),
      init_buffer_len_(buffer_capacity),
      buffer_(init_buffer_len_)
  {
  }

  /// Clear the buffer.
  void clear()
  {
    begin_offset_ = 0;
    end_offset_ = 0;
  }

  // Return a pointer to the beginning of the unread data.
  byte_type_pointer data()
  {
    return &buffer_[0] + begin_offset_;
  }

  // Return a pointer to the beginning of the unread data.
  byte_type_const_pointer data() const
  {
    return &buffer_[0] + begin_offset_;
  }

  // Is there no unread data in the buffer.
  bool empty() const
  {
    return begin_offset_ == end_offset_;
  }

  // Return the amount of unread data the is in the buffer.
  size_type size() const
  {
    return end_offset_ - begin_offset_;
  }

  // Consume multiple bytes from the beginning of the buffer.
  void consume(size_type count)
  {
    assert(begin_offset_ + count <= end_offset_);
    begin_offset_ += count;
    if (empty())
      clear();
  }

   void consume(size_type count,std::vector<byte_type>& buff)
  {
    assert(begin_offset_ + count <= end_offset_);
    buff.assign(buffer_.begin()+begin_offset_,buffer_.begin()+begin_offset_+count);
    begin_offset_ += count;
    if (empty())
      clear();
  }

  // Return the maximum size for data in the buffer.
  size_type capacity() const
  {
    return buffer_.size();
  }

  bool full() const
  {
    return end_offset_ == capacity();
  }

  // Return a pointer to the beginning of the unread data.
  byte_type_pointer buff()
  {
    return &buffer_[0] + end_offset_;
  }

  // Return a pointer to the beginning of the unread data.
  byte_type_const_pointer buff() const
  {
    return &buffer_[0] + end_offset_;
  }

  // 可以写的最大长度
  size_type buff_length() const
  {
    return capacity() - end_offset_;
  }

  //  fill multiple bytes into the ending of the buffer.
  void fill(size_type length)
  {
    assert(end_offset_ + length <= capacity());
    end_offset_ = end_offset_ + length;
    if(full()){
      shrink_to_fit();
    }
    if(full()){
      size_type resize_len = capacity() + init_buffer_len_;
      buffer_.resize(resize_len);
    }
  }

  // 
  void shrink_to_fit()
  {
    if (begin_offset_ > 0)
    {
      using namespace std; // For memmove.
      memmove(&buffer_[0], &buffer_[0] + begin_offset_, size());
      end_offset_ = size();
      begin_offset_ = 0;
    }
  }

  

private:
  // The offset to the beginning of the unread data.
  size_type begin_offset_;

  // The offset to the end of the unread data.
  size_type end_offset_;

  // 
  size_type init_buffer_len_;
  
  // The data in the buffer.
  std::vector<byte_type> buffer_;
};

using buffer_t = buffer_storage;
using buffer_ptr = std::shared_ptr<buffer_t>;


} // end namespace Net
} // end namespace Parrot

#endif //BUFFER_STORAGE_H
