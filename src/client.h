/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*   
*   filename：client.h
*   auther  ：wuyingzhen
*   create  ：2022-04-06
*   someother：
*
================================================================*/

#ifndef _CLIENT_H
#define _CLIENT_H

#define ASIO_ENABLE_HANDLER_TRACKING 
#include "asio/detail/noncopyable.hpp"
#include "asio/io_context.hpp"
#include "asio/ip/tcp.hpp"

#include "socket_stream.h"


namespace Parrot {
namespace Net {

/**
 * 1. 明确要实现的功能是第一步
 * 2. 分模块，确定模块接口
 * 3. 跟踪程序流
 * 4. 测试功能
 */

// 异步编程 需要注意： 时效性，因为不确定异步调用的时间，所以过期时间，变量和内存的时效性，错误的返回

//pwd
class Client;
using client_ptr = std::shared_ptr<Client>;


class Client
  : public asio::detail::noncopyable,
  public std::enable_shared_from_this<Client> {
   public:

    using on_connect = std::function<void(client_ptr)>;
    using on_close = std::function<void(client_ptr)>;
    using on_message = std::function<void(asio::mutable_buffer,client_ptr)>;

    Client(asio::io_context& io_context,std::string ip,unsigned short port);
    ~Client() = default;
    void open(on_connect,on_close,on_message);



 private:
  bool do_connect();
  //void set_stream(socket_stream_ptr stream);



  std::string ip_;
  unsigned short port_;
  asio::io_context& io_context_;
  asio::ip::tcp::socket socket_;
  std::unique_ptr<socket_stream> stream_;
  //asio::ip::tcp::resolver resolver_;

  on_connect on_connect_;
  on_close on_close_;
  on_message on_message_;

};


} // end namespace Net
}  // end namespace Parrot

#endif //CLIENT_H
