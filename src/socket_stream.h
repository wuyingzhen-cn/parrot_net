/*================================================================
*   Copyright (C) 2022 dahuangfeng Ltd. All rights reserved.
*
*   filename：socket_stream.h
*   auther  ：wuyingzhen
*   create  ：2022-03-25
*   someother：
*
================================================================*/

#ifndef _SOCKET_STREAM_H
#define _SOCKET_STREAM_H

#include <memory>
#include <forward_list>
#include <functional>
#define ASIO_ENABLE_HANDLER_TRACKING

#include "asio/detail/noncopyable.hpp"
#include "asio/ip/tcp.hpp"
#include "asio/buffer.hpp"
#include "asio/write.hpp"

#include "buffer_storage.h"

namespace Parrot {
namespace Net {

class socket_stream;




class socket_stream
  : public asio::detail::noncopyable
  {
   public:
    //解包函数
using package_parse = std::function<std::pair<bool,std::size_t>(asio::const_buffer)> ;
using on_connect = std::function<void(void)>;
using on_close = std::function<void(void)>;

    enum STATUS {
      SOCKET_CONNECT_STATUS_INIT, //初始化
    SOCKET_CONNECT_STATUS_DISCONNECTING,	//正在断开
    SOCKET_CONNECT_STATUS_DISCONNECTED,		//已经断开
    SOCKET_CONNECT_STATUS_CONNECTING,		//正在连接
    SOCKET_CONNECT_STATUS_CONNECTED			//连接成功
  };
  socket_stream(asio::ip::tcp::socket socket);

  bool open(on_connect,on_close,package_parse);
  int send(buffer_t send_buf);
  int read(buffer_t read_buf);
  bool close();

 private:

  void do_read();
  void do_write();

  bool on_write(std::error_code ec, std::size_t bytes_transferred);
  bool on_read(std::error_code ec, std::size_t bytes_transferred);

  void stop();

  asio::ip::tcp::socket socket_;
  enum STATUS status_;

  // for send
  std::forward_list<buffer_ptr> send_buffer_pool_;
  bool is_sending_;
  buffer_ptr current_send_buffer_;

  // for read
  buffer_ptr read_buffer_;
  //解包
  package_parse package_parse_;
  on_close on_close_;
  on_connect on_connect_;
 
};

} // end namespace Net
} // end namespace Parrot

#endif //SOCKET_STREAM_H
