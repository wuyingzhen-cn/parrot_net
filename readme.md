## 网络库
在 asio 网络库基础上
---
拥有多个agent;
生命周期包含所有agent的声明周期
server{
    agentManager;
    ipv4_or_ipv6;
    tcp_or_udp;
    message_protocal_type; //http,websocket,customed 根据字节流解析协议,组装协议
    serialize;  //序列化

    broadcast();

}


--- 
客户端在服务器端的代理; 1v1
* agent status:
连接成功;
有效的;  //接受到一个成功消息,或者有认证消息
close;  // 关闭
最后连接时间等等
* agent construct:
应用层消息协议:解包,打包
序列化,反序列化
输出函数:当agent收到一个完整消息后,输出到外界;

agent{
    (message_protocal_type,serialize,on_msg)
    status: 
    send_msg(msg);
    
}

输出:
on_msg(agent&,msg&);

输入:
send_msg(msg);

## 定时
(time,callback)

## 业务流程
service 都是单例,不可copyable